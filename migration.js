require('dotenv').config();

const mysql = require('mysql');
const migrations = require('mysql-migrations');

const {
    MIGRATION_MYSQLDB_URL,
    MIGRATION_MYSQLDB_USERNAME,
    MIGRATION_MYSQLDB_PASSWORD,
    MIGRATION_MYSQLDB_NAME,
    MIGRATION_MYSQLDB_PORT,
} = process.env;

async function execute() {
    const connection = mysql.createPool({
        connectionLimit: 10,
        host: MIGRATION_MYSQLDB_URL || 'localhost',
        user: MIGRATION_MYSQLDB_USERNAME || 'root',
        password: MIGRATION_MYSQLDB_PASSWORD || 'Password123',
        database: MIGRATION_MYSQLDB_NAME || 'moscorddb',
        port: MIGRATION_MYSQLDB_PORT || '3306',

    });

    migrations.init(connection, `${__dirname}/migrations`);
}

execute().then();
