const up =
    'CREATE TABLE `inventory` ( ' +
    '  `id` int(11) NOT NULL AUTO_INCREMENT, ' +
    '  `seller_id` int(11) NOT NULL, ' +
    '  `product_id` int(11) NOT NULL, ' +
    '  `qty` bigint(11) NOT NULL, ' +
    '  `created_at` DATETIME NOT NULL DEFAULT NOW(),  ' +
    '  `updated_at` DATETIME NOT NULL DEFAULT NOW(), ' +
    '  `deleted_at` DATETIME NULL, ' +
    '  PRIMARY KEY (`id`), ' +
    '  CONSTRAINT inventory_product_id_foreign FOREIGN KEY (product_id) REFERENCES products(id), ' +
    '  CONSTRAINT inventory_seller_id_foreign FOREIGN KEY (seller_id) REFERENCES sellers(id));';

const down = 'DROP TABLE `inventory`;';

module.exports = {
    up,
    down
};
