const up =
    'CREATE TABLE `products` ( ' +
    '  `id` int(11) NOT NULL AUTO_INCREMENT, ' +
    '  `item_name` VARCHAR(50) NOT NULL, ' +
    '  `created_at` DATETIME NOT NULL DEFAULT NOW(),  ' +
    '  `updated_at` DATETIME NOT NULL DEFAULT NOW(), ' +
    '  `deleted_at` DATETIME NULL, ' +
    '  CONSTRAINT product_pkey PRIMARY KEY (id));';

const down = 'DROP TABLE `products`;';

module.exports = {
    up,
    down
};
