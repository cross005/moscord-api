const up =
    'CREATE TABLE `sellers` ( ' +
    '  `id` int(11) NOT NULL AUTO_INCREMENT, ' +
    '  `first_name` VARCHAR(50) NOT NULL, ' +
    '  `last_name` VARCHAR(50) NOT NULL, ' +
    '  `created_at` DATETIME NOT NULL DEFAULT NOW(),  ' +
    '  `updated_at` DATETIME NOT NULL DEFAULT NOW(), ' +
    '  `deleted_at` DATETIME NULL, ' +
    '  CONSTRAINT seller_pkey PRIMARY KEY (id));';

const down = 'DROP TABLE `sellers`;';

module.exports = {
    up,
    down
};
