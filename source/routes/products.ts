import express from 'express';
import ProductController from '../controllers/Product';

const router = express.Router();

router.post('/create-product', ProductController.create_product);
router.put('/update-product', ProductController.update_product);
router.delete('/delete-product', ProductController.delete_product);

export = router;
