import express from 'express';
import SellerController from '../controllers/Seller';

const router = express.Router();

router.post('/create-seller', SellerController.create_seller);
router.put('/update-seller', SellerController.update_seller);
router.delete('/delete-seller', SellerController.delete_seller);

export = router;
