import express from 'express';
import CartController from '../controllers/Cart';

const router = express.Router();

router.post('/add-to-cart', CartController.add_to_cart);
router.post('/checkout', CartController.checkout);

export = router;
