import { NextFunction, Request, Response } from 'express';
import { Connect, InsertBulk, Query } from '../config/mysql';

const add_to_cart = async (req: Request, res: Response, next: NextFunction) => {
    let { seller_id, products } = req.body;
    let values: any[][] = [];

    products.forEach((element: any) => {
        values.push([seller_id, element.product_id, element.qty]);
    });

    console.table(values);

    let query = `INSERT INTO cart (seller_id, product_id, qty) VALUES ?`;

    Connect()
        .then((connection) => {
            InsertBulk(query, [values])
                .then((result) => {
                    return res.status(200).json({
                        result
                    });
                })
                .catch((error) => {
                    return res.status(200).json({
                        message: error.message,
                        error
                    });
                })
                .finally(() => {
                    connection.end();
                });
        })
        .catch((error) => {
            return res.status(200).json({
                message: error.message,
                error
            });
        });
};

const checkout = async (req: Request, res: Response, next: NextFunction) => {
    let { seller_id } = req.body;

    let query = `SELECT c.id as order_id, s.first_name as seller_name, p.item_name as product_name, c.qty as quantity, c.created_at as order_date
                FROM cart c 
                LEFT JOIN sellers s ON c.seller_id = s.id 
                LEFT JOIN products p ON c.product_id = p.id
                WHERE c.seller_id = ${seller_id};`;

    Connect()
        .then((connection) => {
            Query(connection, query)
                .then((result) => {
                    return res.status(200).json({
                        result
                    });
                })
                .catch((error) => {
                    return res.status(200).json({
                        message: error.message,
                        error
                    });
                })
                .finally(() => {
                    connection.end();
                });
        })
        .catch((error) => {
            return res.status(200).json({
                message: error.message,
                error
            });
        });
};

export default { add_to_cart, checkout };
