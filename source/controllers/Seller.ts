import { NextFunction, Request, Response } from 'express';
import { Connect, Query } from '../config/mysql';

const create_seller = async (req: Request, res: Response, next: NextFunction) => {
    let { first_name, last_name } = req.body;

    let query = `INSERT INTO sellers (first_name, last_name) VALUES ("${first_name}", "${last_name}")`;

    Connect()
        .then((connection) => {
            Query(connection, query)
                .then((result) => {
                    return res.status(200).json({
                        result
                    });
                })
                .catch((error) => {
                    return res.status(200).json({
                        message: error.message,
                        error
                    });
                })
                .finally(() => {
                    connection.end();
                });
        })
        .catch((error) => {
            return res.status(200).json({
                message: error.message,
                error
            });
        });
};

const update_seller = async (req: Request, res: Response, next: NextFunction) => {
    let { id, first_name, last_name } = req.body;

    let query = `UPDATE sellers set first_name = "${first_name}", last_name = "${last_name}" WHERE id = ${id}`;

    Connect()
        .then((connection) => {
            Query(connection, query)
                .then((result) => {
                    return res.status(200).json({
                        result
                    });
                })
                .catch((error) => {
                    return res.status(200).json({
                        message: error.message,
                        error
                    });
                })
                .finally(() => {
                    connection.end();
                });
        })
        .catch((error) => {
            return res.status(200).json({
                message: error.message,
                error
            });
        });
};

const delete_seller = async (req: Request, res: Response, next: NextFunction) => {
    let { id } = req.body;

    let query = `DELETE FROM sellers WHERE id = ${id}`;

    Connect()
        .then((connection) => {
            Query(connection, query)
                .then((result) => {
                    return res.status(200).json({
                        result
                    });
                })
                .catch((error) => {
                    return res.status(200).json({
                        message: error.message,
                        error
                    });
                })
                .finally(() => {
                    connection.end();
                });
        })
        .catch((error) => {
            return res.status(200).json({
                message: error.message,
                error
            });
        });
};

export default { create_seller, update_seller, delete_seller };
