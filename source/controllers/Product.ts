import { NextFunction, Request, Response } from 'express';
import { Connect, Query } from '../config/mysql';

const create_product = async (req: Request, res: Response, next: NextFunction) => {
    let { item_name } = req.body;

    let query = `INSERT INTO products (item_name) VALUES ("${item_name}")`;

    Connect()
        .then((connection) => {
            Query(connection, query)
                .then((result) => {
                    return res.status(200).json({
                        result
                    });
                })
                .catch((error) => {
                    return res.status(200).json({
                        message: error.message,
                        error
                    });
                })
                .finally(() => {
                    connection.end();
                });
        })
        .catch((error) => {
            return res.status(200).json({
                message: error.message,
                error
            });
        });
};

const update_product = async (req: Request, res: Response, next: NextFunction) => {
    let { id, item_name } = req.body;

    let query = `UPDATE products set item_name = "${item_name}" WHERE id = ${id}`;

    Connect()
        .then((connection) => {
            Query(connection, query)
                .then((result) => {
                    return res.status(200).json({
                        result
                    });
                })
                .catch((error) => {
                    return res.status(200).json({
                        message: error.message,
                        error
                    });
                })
                .finally(() => {
                    connection.end();
                });
        })
        .catch((error) => {
            return res.status(200).json({
                message: error.message,
                error
            });
        });
};

const delete_product = async (req: Request, res: Response, next: NextFunction) => {
    let { id } = req.body;

    let query = `DELETE FROM products WHERE id = ${id}`;

    Connect()
        .then((connection) => {
            Query(connection, query)
                .then((result) => {
                    return res.status(200).json({
                        result
                    });
                })
                .catch((error) => {
                    return res.status(200).json({
                        message: error.message,
                        error
                    });
                })
                .finally(() => {
                    connection.end();
                });
        })
        .catch((error) => {
            return res.status(200).json({
                message: error.message,
                error
            });
        });
};

export default { create_product, update_product, delete_product };
